// This is our server na
// Require allows Express package to be inputed.
const express = require("express");
// invoking the express package is to create a server/api and save it 
// to a variable which we can refer to later to creat routes.
const app = express();
// express.json() is a method that allows us to handle a stream of data from our client and receive a data and automatically parsed the incoming JSON from request.

// app.use() used to run another fucntion or method in our expressjs api
// It is used to run middlewares (function that add features to our applciation)
app.use(express.json());

// you can directly input 4000 to the listen method.
const port = 4000;


// / Mock collection of courses:
let courses = [

	{
		name: "Python 101",
		description: "Learn Python",
		price: 25000
	},
	{
		name: "ReactJS 101",
		description: "Learn ReactJS",
		price: 35000
	},
	{
		name: "ExpressJS 101",
		description: "Learn ExpressJS",
		price: 28000
	}


];

let users = [

	{
		email: "marybell_knight",
		password: "merrymarybell"
	},
	{
		email: "janedoePriest",
		password: "jobPriest100"
	},
	{
		email: "kimToFu",
		password: "dubuToFu"
	}
];


// Creating a route in express:
// access express to have access to its route methods
app.get('/',(req,res)=>{
	// res.send() is similar to response.end() and automatically creates and add the headers.
	res.send("Hello from our first ExpressJS route!");
})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS POST Method Route!");
})

app.put('/',(req,res)=>{
	res.send("Hello from our first ExpressJS PUT Method Route!");
})

app.delete('/',(req,res)=>{
	res.send("Hello from our first ExpressJS DELETE Method Route!");
})


app.get('/courses',(req,res)=>{
	res.send(courses);
})


app.post('/courses',(req,res)=>{

// here matic nagpaparsed na ung JSON file.
// Note: everytime you need to access or use the request body, log it in the console first.
	console.log(req.body);
// add the input as req.body of the request
	courses.push(req.body)

// send the updated courses to the client
	res.send(courses);
	//console.log(courses);
});


app.get('/users',(req,res)=>{
	res.send(users);
})

app.post('/users',(req,res)=>{
	console.log(req.body);
	users.push(req.body)
	res.send(users);

});

// Stretch Goal #1
app.delete('/users',(req,res)=>{
	console.log(req.body);
	users.pop(req.body);
	res.send(users);
})
// stretch #2
app.put('/courses',(req,res)=>{

})

// listen() assign a port and send message.
app.listen(port,() => console.log(`Express API running at port 4000`))